package ru.msu.nosql;

import java.util.Arrays;
import java.util.Date;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import ru.msu.nosql.model.Person;

@RestController
public class HelloController {

    @GetMapping("/hello")
    public String hello(@RequestParam("name") String name) {
        return "Hello " + name;
    }

    @GetMapping("/person")
    public List<Person> listPersons() {
//        return mongoTemplate.findAll(Person.class, "person");
        return Arrays.asList(
                new Person("p1", "John Smith", new Date()),
                new Person("p2", "Steve Brown", new Date()),
                new Person("p3", "Jack London", new Date())
        );
    }
}
