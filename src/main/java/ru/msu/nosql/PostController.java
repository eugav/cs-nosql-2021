package ru.msu.nosql;

import java.util.List;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import ru.msu.nosql.model.Post;
import ru.msu.nosql.model.Student;
import ru.msu.nosql.repository.PostRepository;
import ru.msu.nosql.repository.StudentRepository;

@RestController
public class PostController {

    private final PostRepository postRepository;

    public PostController(PostRepository postRepository) {
        this.postRepository = postRepository;
    }

    @GetMapping("/post/{id}")
    public Post findById(@PathVariable("id") String id) {
        return postRepository.getById(id);
    }

    @GetMapping("/post")
    public List<Post> search(@RequestParam("query") String query) {
        return postRepository.search(query);
    }

    @PostMapping("/post")
    public Post createNewPost(@RequestBody Post post) {
        return postRepository.addPost(post);
    }

}
