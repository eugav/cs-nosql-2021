package ru.msu.nosql;

import com.hazelcast.config.Config;
import com.hazelcast.config.EvictionConfig;
import com.hazelcast.config.EvictionPolicy;
import com.hazelcast.config.MapConfig;
import com.hazelcast.config.MaxSizePolicy;
import com.hazelcast.core.Hazelcast;
import com.hazelcast.core.HazelcastInstance;
import java.util.Arrays;
import org.apache.http.HttpHost;
import org.elasticsearch.client.RestClient;
import org.elasticsearch.client.RestHighLevelClient;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class NosqlSampleApplication {

	@Bean
	public RestHighLevelClient elasticsearchClient(@Value("${elasticsearch.hosts}") String elasticHosts) {
		String[] hosts = elasticHosts.split(",");
		HttpHost[] httpHosts = Arrays.stream(hosts)
				.map(HttpHost::create).toArray(HttpHost[]::new);
		return new RestHighLevelClient(RestClient.builder(httpHosts));
	}

	@Bean
	public HazelcastInstance hazelcastInstance() {
		Config cfg = new Config("sample_app").addMapConfig(
				new MapConfig("student").setEvictionConfig(
						new EvictionConfig().setSize(100)
								.setMaxSizePolicy(MaxSizePolicy.PER_NODE)
								.setEvictionPolicy(EvictionPolicy.LRU)
				)
		);
		return Hazelcast.newHazelcastInstance(cfg);
	}

	public static void main(String[] args) {
		SpringApplication.run(NosqlSampleApplication.class, args);
	}

}
