package ru.msu.nosql;

import java.util.List;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import ru.msu.nosql.model.Student;
import ru.msu.nosql.repository.StudentRepository;

@RestController
public class StudentController {

    private final StudentRepository studentRepository;

    public StudentController(@Qualifier("hazelcastCachedStudentRepository") StudentRepository studentRepository) {
        this.studentRepository = studentRepository;
    }

    @GetMapping("/student")
    public List<Student> findAll() {
        return studentRepository.findAll();
    }

    @GetMapping("/student/{id}")
    public Student findById(@PathVariable("id") String id) {
        return studentRepository.findById(id);
    }

    @PostMapping("/student")
    public Student createNewStudent(@RequestBody Student student) {
        Student newStudent = new Student(null, student.getName(), student.getAge(), student.getAddresses());
        return studentRepository.save(newStudent);
    }

    @PutMapping("/student/{id}")
    public Student createNewStudent(@PathVariable("id") String id, @RequestBody Student student) {
        Student existingStudent = studentRepository.findById(id);
        if (existingStudent != null) {
            existingStudent.setName(student.getName());
            existingStudent.setAge(student.getAge());
            existingStudent.setAddresses(student.getAddresses());
        }
        return studentRepository.save(existingStudent);
    }

    @DeleteMapping("/student/{id}")
    public void deleteStudent(@PathVariable("id") String id) {
        studentRepository.delete(id);
    }
}
