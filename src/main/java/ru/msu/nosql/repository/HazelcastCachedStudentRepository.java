package ru.msu.nosql.repository;

import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.map.IMap;
import com.hazelcast.topic.ITopic;
import java.util.List;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;
import ru.msu.nosql.model.Student;

@Repository
public class HazelcastCachedStudentRepository implements StudentRepository {

    private final StudentRepository delegate;

    private final IMap<String, Student> studentsCache;

    private final ITopic<Student> studentUpdates;

    public HazelcastCachedStudentRepository(
            @Qualifier("mongoStudentRepository") StudentRepository delegate,
            HazelcastInstance hazelcastInstance
    ) {
        this.delegate = delegate;
        studentsCache = hazelcastInstance.getMap("student");
        studentUpdates = hazelcastInstance.getTopic("studentUpdates");
        studentUpdates.addMessageListener(message -> System.out.println(
                message.getPublishTime() + " " + message.getPublishingMember() + message.getMessageObject())
        );
    }

    @Override
    public List<Student> findAll() {
        return delegate.findAll();
    }

    @Override
    public Student findById(String id) {
        return studentsCache.computeIfAbsent(id, delegate::findById);
    }

    @Override
    public Student save(Student student) {
        studentsCache.lock(student.getId());
        try {
            Student updated = delegate.save(student);
            studentsCache.put(student.getId(), student);
            studentUpdates.publish(student);
            return updated;
        } finally {
            studentsCache.unlock(student.getId());
        }
    }

    @Override
    public void delete(String id) {
        delegate.delete(id);
        studentsCache.remove(id);
    }
}
