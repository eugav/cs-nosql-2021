package ru.msu.nosql.repository;

import java.util.List;
import ru.msu.nosql.model.Student;

public interface StudentRepository {
    List<Student> findAll();

    Student findById(String id);

    Student save(Student student);

    void delete(String id);
}
