package ru.msu.nosql.repository;

import com.github.benmanes.caffeine.cache.Cache;
import com.github.benmanes.caffeine.cache.Caffeine;
import java.time.Duration;
import java.util.List;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;
import ru.msu.nosql.model.Student;

@Repository
public class CachedStudentRepository implements StudentRepository {

    private final StudentRepository delegate;

    private final Cache<String, Student> cache;

    public CachedStudentRepository(@Qualifier("mongoStudentRepository") StudentRepository delegate) {
        this.delegate = delegate;
        this.cache = Caffeine.newBuilder()
                .maximumSize(100)
                .expireAfterAccess(Duration.ofHours(12))
                .expireAfterWrite(Duration.ofHours(24))
                .build(delegate::findById);
    }

    @Override
    public List<Student> findAll() {
        return delegate.findAll();
    }

    @Override
    public Student findById(String id) {
        return cache.asMap().get(id);
    }

    @Override
    public Student save(Student student) {
        Student updated = delegate.save(student);
        cache.put(student.getId(), student);
        return updated;
    }

    @Override
    public void delete(String id) {
        delegate.delete(id);
        cache.invalidate(id);
    }
}
