package ru.msu.nosql.repository;

import java.util.List;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;
import ru.msu.nosql.model.Student;

@Repository
public class MongoStudentRepository implements StudentRepository {

    private static final String COLLECTION_NAME = "student";

    private MongoTemplate mongoTemplate;

    public MongoStudentRepository(MongoTemplate mongoTemplate) {
        this.mongoTemplate = mongoTemplate;
    }

    @Override
    public List<Student> findAll() {
        return mongoTemplate.findAll(Student.class, COLLECTION_NAME);
    }

    @Override
    public Student findById(String id) {
        return mongoTemplate.findById(id, Student.class, COLLECTION_NAME);
    }

    @Override
    public Student save(Student student) {
        return mongoTemplate.save(student, COLLECTION_NAME);
    }

    @Override
    public void delete(String id) {
        Query query = Query.query(Criteria.where("_id").is(id));
        mongoTemplate.remove(query, COLLECTION_NAME);
    }
}
