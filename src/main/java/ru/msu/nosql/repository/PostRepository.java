package ru.msu.nosql.repository;

import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import org.elasticsearch.action.get.GetRequest;
import org.elasticsearch.action.get.GetResponse;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.springframework.stereotype.Repository;
import ru.msu.nosql.model.Post;

@Repository
public class PostRepository {

    private final RestHighLevelClient elasticSearchClient;

    public PostRepository(RestHighLevelClient elasticSearchClient) {
        this.elasticSearchClient = elasticSearchClient;
    }

    public List<Post> search(String query) {
        SearchRequest searchRequest = new SearchRequest("so_ru").source(new SearchSourceBuilder()
                .query(QueryBuilders.simpleQueryStringQuery(query)));
        try {
            SearchHit[] hits = elasticSearchClient.search(searchRequest, RequestOptions.DEFAULT).getHits().getHits();
            return Arrays.stream(hits).map( hit ->
                    new Post(hit.getId(), (String) hit.getSourceAsMap().get("Body"))
            ).collect(Collectors.toList());
        } catch (IOException e) {
            throw new RuntimeException(e.getMessage(), e);
        }
    }

    public Post addPost(Post post) {
        Map<String, Object> map = new HashMap<>();
        map.put("Id", post.getId());
        map.put("Body", post.getBody());
        IndexRequest indexRequest = new IndexRequest("so_ru")
                .source(map)
                .id(post.getId());
        try {
            elasticSearchClient.index(indexRequest, RequestOptions.DEFAULT);
        } catch (IOException e) {
            throw new RuntimeException(e.getMessage(), e);
        }
        return post;
    }

    public Post getById(String postId) {
        try {
            GetResponse response = elasticSearchClient.get(new GetRequest("so_ru", postId), RequestOptions.DEFAULT);
            if (!response.isExists()) {
                return null;
            }
            Map<String, Object> source = response.getSourceAsMap();
            return new Post(response.getId(), source.get("Body").toString());
        } catch (IOException e) {
            throw new RuntimeException(e.getMessage(), e);
        }
    }
}
