package ru.msu.nosql.model;

import java.io.Serializable;
import java.util.List;
import org.bson.codecs.pojo.annotations.BsonId;

public class Student implements Serializable {

    private static final long serialVersionUID = -1360070254892429031L;

    @BsonId
    private String id;
    private String name;
    private Integer age;
    private List<Address> addresses;

    public Student(String id, String name, Integer age, List<Address> addresses) {
        this.id = id;
        this.name = name;
        this.age = age;
        this.addresses = addresses;
    }

    public Student() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public List<Address> getAddresses() {
        return addresses;
    }

    public void setAddresses(List<Address> addresses) {
        this.addresses = addresses;
    }

    @Override
    public String toString() {
        return "Student{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                ", age=" + age +
                ", addresses=" + addresses +
                '}';
    }
}
